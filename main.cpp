#include "client_ws.hpp"
#include "server_ws.hpp"
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/json.hpp>
#include <crow.h>

#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <bsoncxx/builder/stream/array.hpp>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/builder/stream/helpers.hpp>
#include <bsoncxx/types.hpp>


using namespace std;

using WsServer = SimpleWeb::SocketServer<SimpleWeb::WS>;
using WsClient = SimpleWeb::SocketClient<SimpleWeb::WS>;
using bsoncxx::builder::stream::document;
using bsoncxx::builder::stream::array;

int main() {

    // WebSocket (WS)-server at port 8080 using 1 thread
    WsServer server;
    server.config.port = 8081;

    // Example 1: echo WebSocket endpoint
    // Added debug messages for example use of the callbacks
    // Test with the following JavaScript:
    //   var ws=new WebSocket("ws://localhost:8080/echo");
    //   ws.onmessage=function(evt){console.log(evt.data);};
    //   ws.send("test");

    auto &echo = server.endpoint["^/dbdata/?$"];
    echo.on_message = []( shared_ptr<WsServer::Connection> connection, shared_ptr<WsServer::Message> message) {

        auto message_str = message->string();
        auto doc = document{};
        // stream::array builds a BSON array
        //auto arr = array{};
        // We append keys and values to documents using the '<<' operator;
        doc << "title"
            << "myValue"
            << "ip"
            << "ipValue"
            << "url"
            << "urlValue"
            << "source "
            << "sourceValue"
            << "tags"
            << "tagValue"
            << "location"
            << "locationValue"
            << "metaData"
            << "metaDataValue"
            << "text"
            << "textValue"
            << "links"
            << "linkValue";

        std::cout << bsoncxx::to_json(doc) << std::endl;
        mongocxx::instance inst{};
        mongocxx::client conn{mongocxx::uri{}};
        bsoncxx::builder::stream::document document{};

        auto collection = conn["testdb"]["testcollection"];

        collection.insert_one(doc.view());

        cout << "Server: Message received: \"" << message_str << "\" from " << connection.get() << endl;
        cout << "Server: Sending message \""   << message_str << "\" to " << connection.get() << endl;

        auto send_stream = make_shared<WsServer::SendStream>();
        *send_stream << message_str;
        // connection->send is an asynchronous function
        connection->send(send_stream, [](const SimpleWeb::error_code &ec) {
            if(ec) {
                cout << "Server: Error sending message. " <<
                     // See http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html, Error Codes for error code meanings
                     "Error: " << ec << ", error message: " << ec.message() << endl;
            }
        });
    };

    echo.on_open = [](shared_ptr<WsServer::Connection> connection) {
        cout << "Server: Opened connection " << connection.get() << endl;
    };

    // See RFC 6455 7.4.1. for status codes
    echo.on_close = [](shared_ptr<WsServer::Connection> connection, int status, const string & /*reason*/) {
        cout << "Server: Closed connection " << connection.get() << " with status code " << status << endl;
    };

    // See http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html, Error Codes for error code meanings
    echo.on_error = [](shared_ptr<WsServer::Connection> connection, const SimpleWeb::error_code &ec) {
        cout << "Server: Error in connection " << connection.get() << ". "
             << "Error: " << ec << ", error message: " << ec.message() << endl;
    };

    thread server_thread([&server]() {

        std::cout << "Server is started on: " << server.config.port << std::endl;

        server.start();

    });

    // Wait for server to start so that the client can connect
    //this_thread::sleep_for(chrono::seconds(1));

    thread crow_thread([]() {

        std::cout << "crow server is started on: "  << std::endl;

        crow::SimpleApp app;

        CROW_ROUTE( app , "/")([](){

            return "Hello world";

        });

        app.port(18080).run();

    });

    server_thread.join();
    crow_thread.join();
}